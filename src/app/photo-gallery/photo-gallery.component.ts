import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginFormComponent } from '../login-form/login-form.component';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.scss']
})
export class PhotoGalleryComponent implements OnInit {

  imageData = [
    {
      imagePath: "../../assets/photo_glr/Ant-Man_poster.jpg",
      title: "Ant Man",
      desc: "Scott, a master thief, gains the ability to shrink in scale with the help of a futuristic suit. Now he must rise to the occasion of his superhero status and protect his secret from unsavoury elements."
    },
    {
      imagePath: "../../assets/photo_glr/avenggers_end.jpg",
      title: "Avenger Endgame",
      desc: "After Thanos, an intergalactic warlord, disintegrates half of the universe, the Avengers must reunite and assemble again to reinvigorate their trounced allies and restore balance."
    },
    {
      imagePath: "../../assets/photo_glr/black-panther-marvel-cinematic-universe-1038917.jpg",
      title: "Black Panther",
      desc: "After his father's death, T'Challa returns home to Wakanda to inherit his throne. However, a powerful enemy related to his family threatens to attack his nation."
    },
    {
      imagePath: "../../assets/photo_glr/Captain_America_Civil_War_poster.jpg",
      title: "Captain America Civil War",
      desc: "Friction arises between the Avengers when one group supports the government's decision to implement a law to control their powers while the other opposes it."
    },
    {
      imagePath: "../../assets/photo_glr/CaptainAmericaTheFirstAvengerComicConPoster.jpg",
      title: "Captain America The First Avenger",
      desc: "During World War II, Steve Rogers decides to volunteer in an experiment that transforms his weak body. He must now battle a secret Nazi organisation headed by Johann Schmidt to defend his nation."
    },
    {
      imagePath: "../../assets/photo_glr/Guardians-Of-The-Galaxy.jpg",
      title: "Guardians Of The Galaxy",
      desc: "Peter escapes from the planet Morag with a valuable orb that Ronan the Accuser wants. He eventually forms a group with unwilling heroes to stop Ronan."
    },
    {
      imagePath: "../../assets/photo_glr/infinity_was.jpg",
      title: "Infinity War",
      desc: "The Avengers must stop Thanos, an intergalactic warlord, from getting his hands on all the infinity stones. However, Thanos is prepared to go to any lengths to carry out his insane plan."
    },
    {
      imagePath: "../../assets/photo_glr/Iron_Man_poster.jpg",
      title: "Iron Man",
      desc: "When Tony Stark, an industrialist, is captured, he constructs a high-tech armoured suit to escape. Once he manages to escape, he decides to use his suit to fight against evil forces to save the world."
    },
    {
      imagePath: "../../assets/photo_glr/medium-5-avengers-infinity-war-iron-man-spider-man-poster-12x18-original-imaf5yjvpsegvqsz.jpeg",
      title: "Infinity War",
      desc: "The Avengers must stop Thanos, an intergalactic warlord, from getting his hands on all the infinity stones. However, Thanos is prepared to go to any lengths to carry out his insane plan."
    },
    {
      imagePath: "../../assets/photo_glr/new_avg.jpg",
      title: "Avengers",
      desc: "Marvel's Avengers is a 2020 action role-playing brawler video game developed by Crystal Dynamics and published by Square Enix's European subsidiary."
    },
    {
      imagePath: "../../assets/photo_glr/Official_FFH_US_Poster.jpg",
      title: "Spider Man Homecoming",
      desc: "Peter Parker tries to stop the Vulture from selling weapons made with advanced Chitauri technology while trying to balance his life as an ordinary high school student."
    },
    {
      imagePath: "../../assets/photo_glr/Thor_poster.jpg",
      title: "Thor",
      desc: "Thor is exiled by his father, Odin, the King of Asgard, to the Earth to live among mortals. When he lands on Earth, his trusted weapon Mjolnir is discovered and captured by S.H.I.E.L.D."
    },
    {
      imagePath: "../../assets/photo_glr/Thor_The_Dark_World_poster.jpg",
      title: "Thor -The Dark World",
      desc:"Thor sets out on a journey to defeat Malekith, the leader of the Dark Elves when he returns to Asgard to retrieve a dangerous weapon and fulfill his desire of destroying the Nine Realms."
    },
    {
      imagePath: "../../assets/photo_glr/spider-man-homecoming-movie-poster-marvel-cinematic-universe-1038913.jpg",
      title: "Spider Man Homecoming",
      desc: "Peter Parker tries to stop the Vulture from selling weapons made with advanced Chitauri technology while trying to balance his life as an ordinary high school student."
    }
  ]
  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  openDetails(clickedImagedata: any) {

    const dialogRef = this.dialog.open(LoginFormComponent, {
      width: '400px',
      data: { isLogin: false, imageData: clickedImagedata },
    });
  }

}
