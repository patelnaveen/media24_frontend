import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { LoginFormComponent } from '../login-form/login-form.component';
import { LocalServiceService } from '../service/local-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn: boolean = false;
  userData: any = {};
  isActiveClass: string = "/home";
  logo: any = "../../assets/media-24-logo-png-transparent.png"

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private localService: LocalServiceService,
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('userData')) {
      let loggedInUserData = JSON.parse(this.localService.decryptData(localStorage.getItem('userData')))
      this.userData = loggedInUserData;
      this.isLoggedIn = true;
    }
    setTimeout(() => {
      this.isActiveClass = this.router.url;
    }, 0)
  }

  openMenu(url: any) {
    this.router.navigate([url]);
    this.isActiveClass = url;
  }

  login() {
    const dialogRef = this.dialog.open(LoginFormComponent, {
      width: '400px',
      data: { isLogin: true },
      panelClass: 'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.success) {
        this.isLoggedIn = true;
        this.userData = result.data
        localStorage.setItem("userData", this.localService.encryptData(result.data));
        this.router.navigate(['/home']);
        this.isActiveClass = "/home";
      } else {
        this.isLoggedIn = false;
        this.userData = {};
      }
    });
  }

  logout() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to logut!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.clear()
        this.isLoggedIn = false;
        this.router.navigate(['/home']);
        this.isActiveClass = "/home";
        this.localService.showPopup("You have successfully logged out.", "success")
      }
    })
  }
}
