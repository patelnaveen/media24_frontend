import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LocalServiceService } from './service/local-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  loggedInUserData: any;
  constructor(
    private localService: LocalServiceService,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.loggedInUserData = localStorage.getItem('userData') ? JSON.parse(this.localService.decryptData(localStorage.getItem('userData'))) : '';
    if (state.url.includes('/photo-gallery')) {
      if (this.loggedInUserData.userid) {
        return true;
      } else {
        this.router.navigate(['/']);
        return true;
      }
    } else {
      return true;
    }

  }

}
