import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import Swal from 'sweetalert2';


const SECRET_KEY = 'media@1324'

@Injectable({
  providedIn: 'root'
})
export class LocalServiceService {

  usersDB = [
    { userid: "abc@media.com", password: "abc123", "username": "Tom" },
    { userid: "def@media.com", password: "def123", "username": "Jerry" }
  ]

  constructor() { }

  encryptData(data: any) {
    let encryptedString = CryptoJS.AES.encrypt(JSON.stringify(data), SECRET_KEY);
    return encryptedString.toString()
  }

  decryptData(data: any) {
    let decryptedBytes;
    let decryptedText;
    decryptedBytes = CryptoJS.AES.decrypt(data, SECRET_KEY);
    decryptedText = decryptedBytes.toString(CryptoJS.enc.Utf8);
    return decryptedText
  }

  showPopup(message: any, icon: any) {
    Swal.fire({
      icon: icon,
      title: message,
      showConfirmButton: false,
      timer: 1500
    })
  }
}
