import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalServiceService } from '../service/local-service.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  userForm: any;
  isLogin: any = false;
  imageData: any = {};
  constructor(
    public dialogRef: MatDialogRef<LoginFormComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    private localService: LocalServiceService
  ) {
    this.isLogin = data.isLogin
    this.imageData = data.imageData
  }

  ngOnInit(): void {
    this.userFormControls();
  }

  userFormControls() {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }


  login() {
    let result: any = this.localService.usersDB.filter(filterEdData => filterEdData.userid == this.userForm.value.email && filterEdData.password == this.userForm.value.password)[0];
    if (result) {
      this.dialogRef.close({ success: true, data: result });
    } else {
      this.localService.showPopup("Wrong credentials.", 'warning')
    }
  }
}
